﻿
namespace Winforms21._2
{
    partial class FrmKlant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNaam = new System.Windows.Forms.TextBox();
            this.txtStraat = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPostcode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtGemeente = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTelefoon = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.chkHardware = new System.Windows.Forms.CheckBox();
            this.chkInternet = new System.Windows.Forms.CheckBox();
            this.btnAnnuleren = new System.Windows.Forms.Button();
            this.chkSoftware = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chkMulitmedia = new System.Windows.Forms.CheckBox();
            this.btnOpslaan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Naam";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Straat en Nr";
            // 
            // txtNaam
            // 
            this.txtNaam.Location = new System.Drawing.Point(153, 29);
            this.txtNaam.Name = "txtNaam";
            this.txtNaam.Size = new System.Drawing.Size(528, 23);
            this.txtNaam.TabIndex = 2;
            // 
            // txtStraat
            // 
            this.txtStraat.Location = new System.Drawing.Point(153, 58);
            this.txtStraat.Name = "txtStraat";
            this.txtStraat.Size = new System.Drawing.Size(528, 23);
            this.txtStraat.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Postcode";
            // 
            // txtPostcode
            // 
            this.txtPostcode.Location = new System.Drawing.Point(153, 87);
            this.txtPostcode.Name = "txtPostcode";
            this.txtPostcode.Size = new System.Drawing.Size(206, 23);
            this.txtPostcode.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(378, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Gemeente";
            // 
            // txtGemeente
            // 
            this.txtGemeente.Location = new System.Drawing.Point(472, 87);
            this.txtGemeente.Name = "txtGemeente";
            this.txtGemeente.Size = new System.Drawing.Size(209, 23);
            this.txtGemeente.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(33, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Telefoon";
            // 
            // txtTelefoon
            // 
            this.txtTelefoon.Location = new System.Drawing.Point(153, 117);
            this.txtTelefoon.Name = "txtTelefoon";
            this.txtTelefoon.Size = new System.Drawing.Size(528, 23);
            this.txtTelefoon.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 15);
            this.label6.TabIndex = 10;
            this.label6.Text = "E-mail";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(153, 146);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(528, 23);
            this.txtEmail.TabIndex = 11;
            // 
            // chkHardware
            // 
            this.chkHardware.AutoSize = true;
            this.chkHardware.Location = new System.Drawing.Point(77, 259);
            this.chkHardware.Name = "chkHardware";
            this.chkHardware.Size = new System.Drawing.Size(77, 19);
            this.chkHardware.TabIndex = 12;
            this.chkHardware.Text = "Hardware";
            this.chkHardware.UseVisualStyleBackColor = true;
            // 
            // chkInternet
            // 
            this.chkInternet.AutoSize = true;
            this.chkInternet.Location = new System.Drawing.Point(333, 259);
            this.chkInternet.Name = "chkInternet";
            this.chkInternet.Size = new System.Drawing.Size(67, 19);
            this.chkInternet.TabIndex = 13;
            this.chkInternet.Text = "Internet";
            this.chkInternet.UseVisualStyleBackColor = true;
            // 
            // btnAnnuleren
            // 
            this.btnAnnuleren.Location = new System.Drawing.Point(571, 256);
            this.btnAnnuleren.Name = "btnAnnuleren";
            this.btnAnnuleren.Size = new System.Drawing.Size(110, 23);
            this.btnAnnuleren.TabIndex = 14;
            this.btnAnnuleren.Text = "Annuleren";
            this.btnAnnuleren.UseVisualStyleBackColor = true;
            this.btnAnnuleren.Click += new System.EventHandler(this.btnAnnuleren_Click);
            // 
            // chkSoftware
            // 
            this.chkSoftware.AutoSize = true;
            this.chkSoftware.Location = new System.Drawing.Point(77, 306);
            this.chkSoftware.Name = "chkSoftware";
            this.chkSoftware.Size = new System.Drawing.Size(72, 19);
            this.chkSoftware.TabIndex = 15;
            this.chkSoftware.Text = "Software";
            this.chkSoftware.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(33, 215);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 15);
            this.label7.TabIndex = 16;
            this.label7.Text = "Mailing m.b.v";
            // 
            // chkMulitmedia
            // 
            this.chkMulitmedia.AutoSize = true;
            this.chkMulitmedia.Location = new System.Drawing.Point(333, 306);
            this.chkMulitmedia.Name = "chkMulitmedia";
            this.chkMulitmedia.Size = new System.Drawing.Size(87, 19);
            this.chkMulitmedia.TabIndex = 17;
            this.chkMulitmedia.Text = "Multimedia";
            this.chkMulitmedia.UseVisualStyleBackColor = true;
            // 
            // btnOpslaan
            // 
            this.btnOpslaan.Location = new System.Drawing.Point(571, 303);
            this.btnOpslaan.Name = "btnOpslaan";
            this.btnOpslaan.Size = new System.Drawing.Size(110, 23);
            this.btnOpslaan.TabIndex = 18;
            this.btnOpslaan.Text = "Opslaan";
            this.btnOpslaan.UseVisualStyleBackColor = true;
            this.btnOpslaan.Click += new System.EventHandler(this.btnOpslaan_Click);
            // 
            // FrmKlant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(718, 366);
            this.Controls.Add(this.btnOpslaan);
            this.Controls.Add(this.chkMulitmedia);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.chkSoftware);
            this.Controls.Add(this.btnAnnuleren);
            this.Controls.Add(this.chkInternet);
            this.Controls.Add(this.chkHardware);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtTelefoon);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtGemeente);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtPostcode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtStraat);
            this.Controls.Add(this.txtNaam);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmKlant";
            this.Text = "Klant";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNaam;
        private System.Windows.Forms.TextBox txtStraat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPostcode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtGemeente;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTelefoon;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.CheckBox chkHardware;
        private System.Windows.Forms.CheckBox chkInternet;
        private System.Windows.Forms.Button btnAnnuleren;
        private System.Windows.Forms.CheckBox chkSoftware;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkMulitmedia;
        private System.Windows.Forms.Button btnOpslaan;
    }
}