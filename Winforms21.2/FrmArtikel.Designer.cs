﻿
namespace Winforms21._2
{
    partial class FrmArtikel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.grbBtw = new System.Windows.Forms.GroupBox();
            this.rad21 = new System.Windows.Forms.RadioButton();
            this.rad6 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtArtikelnummer = new System.Windows.Forms.TextBox();
            this.txtArtikelomschrijving = new System.Windows.Forms.TextBox();
            this.txtPrijsZonderBtw = new System.Windows.Forms.TextBox();
            this.txtBtw = new System.Windows.Forms.TextBox();
            this.txtPrijsMetBtw = new System.Windows.Forms.TextBox();
            this.btnAnnuleren = new System.Windows.Forms.Button();
            this.btnOpslaan = new System.Windows.Forms.Button();
            this.grbBtw.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Artikelnummer";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Artikelomschrijving";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Eenheidsprijs zonder BTW";
            // 
            // grbBtw
            // 
            this.grbBtw.Controls.Add(this.rad21);
            this.grbBtw.Controls.Add(this.rad6);
            this.grbBtw.Location = new System.Drawing.Point(39, 172);
            this.grbBtw.Name = "grbBtw";
            this.grbBtw.Size = new System.Drawing.Size(200, 100);
            this.grbBtw.TabIndex = 3;
            this.grbBtw.TabStop = false;
            this.grbBtw.Text = "BTW percentage";
            // 
            // rad21
            // 
            this.rad21.AutoSize = true;
            this.rad21.Location = new System.Drawing.Point(17, 56);
            this.rad21.Name = "rad21";
            this.rad21.Size = new System.Drawing.Size(47, 19);
            this.rad21.TabIndex = 1;
            this.rad21.TabStop = true;
            this.rad21.Text = "21%";
            this.rad21.UseVisualStyleBackColor = true;
            this.rad21.CheckedChanged += new System.EventHandler(this.rad21_CheckedChanged);
            // 
            // rad6
            // 
            this.rad6.AutoSize = true;
            this.rad6.Location = new System.Drawing.Point(17, 31);
            this.rad6.Name = "rad6";
            this.rad6.Size = new System.Drawing.Size(41, 19);
            this.rad6.TabIndex = 0;
            this.rad6.TabStop = true;
            this.rad6.Text = "6%";
            this.rad6.UseVisualStyleBackColor = true;
            this.rad6.CheckedChanged += new System.EventHandler(this.rad6_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(39, 305);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "BTW";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(39, 350);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(126, 15);
            this.label5.TabIndex = 5;
            this.label5.Text = "Eenheidsprijs met BTW";
            // 
            // txtArtikelnummer
            // 
            this.txtArtikelnummer.Location = new System.Drawing.Point(246, 39);
            this.txtArtikelnummer.Name = "txtArtikelnummer";
            this.txtArtikelnummer.Size = new System.Drawing.Size(211, 23);
            this.txtArtikelnummer.TabIndex = 6;
            // 
            // txtArtikelomschrijving
            // 
            this.txtArtikelomschrijving.Location = new System.Drawing.Point(246, 87);
            this.txtArtikelomschrijving.Name = "txtArtikelomschrijving";
            this.txtArtikelomschrijving.Size = new System.Drawing.Size(211, 23);
            this.txtArtikelomschrijving.TabIndex = 7;
            // 
            // txtPrijsZonderBtw
            // 
            this.txtPrijsZonderBtw.Location = new System.Drawing.Point(246, 129);
            this.txtPrijsZonderBtw.Name = "txtPrijsZonderBtw";
            this.txtPrijsZonderBtw.Size = new System.Drawing.Size(211, 23);
            this.txtPrijsZonderBtw.TabIndex = 8;
            this.txtPrijsZonderBtw.TextChanged += new System.EventHandler(this.txtPrijsZonderBtw_TextChanged);
            // 
            // txtBtw
            // 
            this.txtBtw.Location = new System.Drawing.Point(246, 302);
            this.txtBtw.Name = "txtBtw";
            this.txtBtw.Size = new System.Drawing.Size(211, 23);
            this.txtBtw.TabIndex = 9;
            // 
            // txtPrijsMetBtw
            // 
            this.txtPrijsMetBtw.Location = new System.Drawing.Point(246, 347);
            this.txtPrijsMetBtw.Name = "txtPrijsMetBtw";
            this.txtPrijsMetBtw.Size = new System.Drawing.Size(211, 23);
            this.txtPrijsMetBtw.TabIndex = 10;
            // 
            // btnAnnuleren
            // 
            this.btnAnnuleren.Location = new System.Drawing.Point(482, 302);
            this.btnAnnuleren.Name = "btnAnnuleren";
            this.btnAnnuleren.Size = new System.Drawing.Size(75, 23);
            this.btnAnnuleren.TabIndex = 11;
            this.btnAnnuleren.Text = "Annuleren";
            this.btnAnnuleren.UseVisualStyleBackColor = true;
            this.btnAnnuleren.Click += new System.EventHandler(this.btnAnnuleren_Click);
            // 
            // btnOpslaan
            // 
            this.btnOpslaan.Location = new System.Drawing.Point(482, 347);
            this.btnOpslaan.Name = "btnOpslaan";
            this.btnOpslaan.Size = new System.Drawing.Size(75, 23);
            this.btnOpslaan.TabIndex = 12;
            this.btnOpslaan.Text = "Opslaan";
            this.btnOpslaan.UseVisualStyleBackColor = true;
            this.btnOpslaan.Click += new System.EventHandler(this.btnOpslaan_Click);
            // 
            // FrmArtikel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 412);
            this.Controls.Add(this.btnOpslaan);
            this.Controls.Add(this.btnAnnuleren);
            this.Controls.Add(this.txtPrijsMetBtw);
            this.Controls.Add(this.txtBtw);
            this.Controls.Add(this.txtPrijsZonderBtw);
            this.Controls.Add(this.txtArtikelomschrijving);
            this.Controls.Add(this.txtArtikelnummer);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.grbBtw);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmArtikel";
            this.Text = "Artikel";
            this.grbBtw.ResumeLayout(false);
            this.grbBtw.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox grbBtw;
        private System.Windows.Forms.RadioButton rad21;
        private System.Windows.Forms.RadioButton rad6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtArtikelnummer;
        private System.Windows.Forms.TextBox txtArtikelomschrijving;
        private System.Windows.Forms.TextBox txtPrijsZonderBtw;
        private System.Windows.Forms.TextBox txtBtw;
        private System.Windows.Forms.TextBox txtPrijsMetBtw;
        private System.Windows.Forms.Button btnAnnuleren;
        private System.Windows.Forms.Button btnOpslaan;
    }
}