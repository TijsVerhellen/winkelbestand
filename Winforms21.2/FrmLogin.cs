﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Winforms21._2
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        string[,] keys = { { "Mia Lodewijks", "1234" }, { "Eric Delcoigne", "wachtwoord" }, { "Tijs Verhellen", "987654321" } };

        

        public string Naam { get { return txtNaam.Text; } }

        private void btnOK_Click(object sender, EventArgs e)
        {
            for (int i = 0; i <= keys.GetUpperBound(0); i++)
            {
                if (txtNaam.Text == keys[i, 0] && txtWachtwoord.Text == keys[i, 1])
                {
                    this.DialogResult = DialogResult.OK;
                }
            }

        }

        private void btnAnnuleren_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
