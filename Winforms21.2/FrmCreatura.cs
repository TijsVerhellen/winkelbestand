﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Winforms21._2
{
    public partial class FrmCreatura : Form
    {
        public FrmCreatura()
        {
            InitializeComponent();
        }

        private FrmLogin loginscherm;
        private FrmArtikel artikel;
        private FrmKlant klant;
        private FrmOver over;
        public string gebruiker;


        private void FrmCreatura_Load(object sender, EventArgs e)
        {
            loginscherm = new FrmLogin();
            
            if (loginscherm.ShowDialog() == DialogResult.OK)
            {
                gebruiker = loginscherm.Naam;
                this.Text = $"Creatura - {gebruiker}";
            }
            else
            {
                Application.Exit();
            }
        }

        private void mnuBestandArtikel_Click(object sender, EventArgs e)
        {
            artikel = new FrmArtikel();
            artikel.Text += $" - {gebruiker}";
            artikel.ShowDialog();
        }

        private void mnuBestandKlant_Click(object sender, EventArgs e)
        {
            klant = new FrmKlant();
            klant.Text += $" - {gebruiker}";
            klant.ShowDialog();
        }

        private void mnuBestandAndere_Click(object sender, EventArgs e)
        {
            loginscherm = new FrmLogin();

            if (loginscherm.ShowDialog() == DialogResult.OK)
            {
                gebruiker = loginscherm.Naam;
                this.Text += $" - {gebruiker}";
            }
            else
            {
                Application.Exit();
            }
        }

        private void mnuBestandAfsluiten_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void mnuHelpOver_Click(object sender, EventArgs e)
        {
            over = new FrmOver();
            over.Text += $" - {gebruiker}";
            over.Show();
            this.AddOwnedForm(over);
        }
    }
}
