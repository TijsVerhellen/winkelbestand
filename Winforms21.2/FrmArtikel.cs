﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Winforms21._2
{
    public partial class FrmArtikel : Form
    {
        public FrmArtikel()
        {
            InitializeComponent();
        }

        private void rad6_CheckedChanged(object sender, EventArgs e)
        {
            if (rad6.Checked)
            {
                herberekenen();
            }
        }

        private void rad21_CheckedChanged(object sender, EventArgs e)
        {
            if (rad21.Checked)
            {
                herberekenen();
            }
        }

        private void btnAnnuleren_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnOpslaan_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtArtikelnummer.Text) && !string.IsNullOrEmpty(txtArtikelomschrijving.Text) && !string.IsNullOrEmpty(txtPrijsZonderBtw.Text) && !string.IsNullOrEmpty(txtBtw.Text) && !string.IsNullOrEmpty(txtPrijsMetBtw.Text))
            {
                List<string> artikels = new List<string>();
                if (File.Exists("Artikels.txt"))
                {
                    using (StreamReader lezen = new StreamReader("Artikels.txt"))
                    {
                        while(!lezen.EndOfStream)
                        {
                            artikels.Add(lezen.ReadLine());
                        }
                    }
                }
                using (StreamWriter opslag = new StreamWriter("Artikels.txt"))
                {
                    foreach (string artikel in artikels)
                    {
                        opslag.WriteLine(artikel);
                    }
                    opslag.WriteLine($"{txtArtikelnummer.Text}|{txtArtikelomschrijving.Text}|{txtPrijsZonderBtw.Text}|{txtBtw.Text}|{txtPrijsMetBtw.Text}");
                }
                txtArtikelnummer.Text = "";
                txtArtikelomschrijving.Text = "";
                txtPrijsZonderBtw.Text = "";
                txtBtw.Text = "";
                txtPrijsMetBtw.Text = "";
            }
            else
            {
                MessageBox.Show("Vul alle velden in!", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void txtPrijsZonderBtw_TextChanged(object sender, EventArgs e)
        {
            if (rad21.Checked || rad6.Checked)
            {
                herberekenen();
            }
        }

        private void herberekenen()
        {
            if (!string.IsNullOrEmpty(txtPrijsZonderBtw.Text) && double.TryParse(txtPrijsZonderBtw.Text.Replace('.', ','), out double prijs))
            {
                if (rad6.Checked)
                {

                    txtBtw.Text = (prijs * 1.06 - prijs).ToString("0.00");
                    prijs *= 1.06;
                }
                else
                {

                    txtBtw.Text = (prijs * 1.21 - prijs).ToString("0.00");
                    prijs *= 1.21;
                }
                txtPrijsMetBtw.Text = prijs.ToString("0.00");
            }
        }
    }
}
