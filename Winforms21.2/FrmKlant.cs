﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Winforms21._2
{
    public partial class FrmKlant : Form
    {
        public FrmKlant()
        {
            InitializeComponent();
        }

        private void btnOpslaan_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNaam.Text) &&
                !string.IsNullOrEmpty(txtStraat.Text) &&
                !string.IsNullOrEmpty(txtPostcode.Text) &&
                !string.IsNullOrEmpty(txtGemeente.Text) &&
                !string.IsNullOrEmpty(txtTelefoon.Text) &&
                !string.IsNullOrEmpty(txtEmail.Text))
            {
                List<string> klanten = new List<string>();
                if (File.Exists("Klanten.txt"))
                {
                    using (StreamReader lezen = new StreamReader("Klanten.txt"))
                    {
                        while(!lezen.EndOfStream)
                        {
                            klanten.Add(lezen.ReadLine());
                        }
                    }
                }
                using(StreamWriter opslag = new StreamWriter("Klanten.txt"))
                {
                    foreach (string klant in klanten)
                    {
                        opslag.WriteLine(klant);
                    }
                    opslag.WriteLine($"{txtNaam.Text}|" +
                        $"{txtStraat.Text}|" +
                        $"{txtPostcode.Text}|" +
                        $"{txtGemeente.Text}|" +
                        $"{txtTelefoon.Text}|" +
                        $"{txtEmail.Text}|" +
                        $"{chkHardware.Checked}|" +
                        $"{chkSoftware.Checked}|" +
                        $"{chkInternet.Checked}|" +
                        $"{chkMulitmedia.Checked}");
                }
                txtNaam.Text = "";
                txtStraat.Text = "";
                txtPostcode.Text = "";
                txtGemeente.Text = "";
                txtTelefoon.Text = "";
                txtEmail.Text = "";
                chkHardware.Checked = false;
                chkSoftware.Checked = false;
                chkInternet.Checked = false;
                chkMulitmedia.Checked = false;
            }
            else
            {
                MessageBox.Show("Vul alle velden in!", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAnnuleren_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
