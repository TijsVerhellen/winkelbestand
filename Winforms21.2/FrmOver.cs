﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Winforms21._2
{
    public partial class FrmOver : Form
    {
        public FrmOver()
        {
            InitializeComponent();
            
        }

        private void btnSluiten_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmOver_Load(object sender, EventArgs e)
        {
            label2.Text = this.Text.Substring(7);
        }
    }
}
